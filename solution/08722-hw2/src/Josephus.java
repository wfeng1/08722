import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * 08722 Data Structures for Application Programmers.
 * Homework Assignment 2
 * Solve Josephus problem with different data structures
 * and different algorithms and compare running times
 *
 * Andrew ID:wfeng1
 * @author Wei Feng
 */
public class Josephus {

    /**
     * Uses ArrayDeque class as Queue/Deque to find the survivor's position.
     * @param size Number of people in the circle that is bigger than 0
     * @param rotation Elimination order in the circle. The value has to be greater than 0
     * @return The position value of the survivor
     */
    public int playWithAD(int size, int rotation) {
        if (size <= 0 || rotation <= 0) {
            throw new RuntimeException("RuntimeException");
        }
        Queue<Integer> queue = new ArrayDeque<Integer>();
        for (int i = 1; i <= size; i++) {
            queue.add(i);
        }
        for (int count = 0; queue.size() > 1;) {
            int tmp = queue.poll();
            count++;
            if (count == rotation) {
                count = 0;
            } else {
                queue.add(tmp);
            }
        }
        return queue.peek();
    }

    /**
     * Uses LinkedList class as Queue/Deque to find the survivor's position.
     * @param size Number of people in the circle that is bigger than 0
     * @param rotation Elimination order in the circle. The value has to be greater than 0
     * @return The position value of the survivor
     */
    public int playWithLL(int size, int rotation) {
        if (size <= 0 || rotation <= 0) {
            throw new RuntimeException("RuntimeException");
        }
        Queue<Integer> queue = new LinkedList<Integer>();
        for (int i = 1; i <= size; i++) {
            queue.add(i);
        }
        for (int count = 0; queue.size() > 1;) {
            int tmp = queue.poll();
            count++;
            if (count == rotation) {
                count = 0;
            } else {
                queue.add(tmp);
            }
        }
        return queue.peek();
    }

    /**
     * Uses LinkedList class to find the survivor's position.
     * However, do NOT use the LinkedList as Queue/Deque
     * Instead, use the LinkedList as "List"
     * That means, it uses index value to find and remove a person to be executed in the circle
     *
     * Note: Think carefully about this method!!
     * When in doubt, please visit one of the office hours!!
     *
     * @param size Number of people in the circle that is bigger than 0
     * @param rotation Elimination order in the circle. The value has to be greater than 0
     * @return The position value of the survivor
     */
    public int playWithLLAt(int size, int rotation) {
        if (size <= 0 || rotation <= 0) {
            throw new RuntimeException("RuntimeException");
        }
        List<Integer> list = new LinkedList<Integer>();
        for (int i = 1; i <= size; i++) {
            list.add(i);
        }
        for (int index = 0; list.size() > 1;) {
            index = (index + rotation - 1) % list.size();
            list.remove(index);
        }
        return list.get(0);
    }

}
