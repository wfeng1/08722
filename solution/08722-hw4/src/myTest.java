import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class myTest {

	public static void main(String[] args) throws IOException {
		
		int size;
        System.out.print("Enter size of hash table (bigger than 0): ");
        size = getInt();
        MyHashTable theHashTable;
        if (size == 0) {
            theHashTable = new MyHashTable();
        } else {
            theHashTable = new MyHashTable(size);
        }

        theHashTable.insert("increase");
        theHashTable.insert("creeping");
        theHashTable.insert("everything");
        theHashTable.insert("ourselves");
        theHashTable.insert("himself");
        theHashTable.insert("finished");
        theHashTable.insert("seventh");
        theHashTable.insert("learned");
        theHashTable.insert("learned");
        theHashTable.insert("creeping");
        theHashTable.insert("receive");
        
        System.out.println("");
        System.out.println("Number of collisions: " + theHashTable.numOfCollisions());
        System.out.println("Number of items: " + theHashTable.size());
        System.out.println("");
        System.out.print("Table: ");
        theHashTable.display();
        
        String testValue = new String("finished");
        if (theHashTable.contains(testValue)) {
            System.out.println("Found: 'finished'");
        } else {
            System.out.println("Cannot find: 'finished'");
        }
        
        System.out.println("Frequency of 'learned': " + theHashTable.showFrequency("learned"));
        System.out.println("Frequency of 'Learned': " + theHashTable.showFrequency("Learned"));

	}
	
	public static int getInt() throws IOException {
        String s = getString();
        return Integer.parseInt(s);
    }
	
	public static String getString() throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String s = br.readLine();
        return s;
    }
}
