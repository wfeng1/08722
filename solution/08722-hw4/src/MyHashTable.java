/**
 * 08722 Data Structures for Application Programmers.
 *
 * Homework Assignment 4
 * HashTable Implementation with linear probing
 *
 * Andrew ID: wfeng1
 * @author Wei Feng
 */
public class MyHashTable implements MyHTInterface {

    // TODO implement constructor with no initial capacity

    // TODO implement constructor with initial capacity

    // TODO implement required methods
	
    /**
     * Instead of using String's hashCode, you are to implement your own here.
     * You need to take the table length into your account in this method.
     *
     * In other words, you are to combine the following two steps into one step.
     * 1. converting Object into integer value
     * 2. compress into the table using modular hashing (division method)
     *
     * Helper method to hash a string for English lowercase alphabet and blank,
     * we have 27 total. But, you can assume that blank will not be added into
     * your table. Refer to the instructions for the definition of words.
     *
     * For example, "cats" : 3*27^3 + 1*27^2 + 20*27^1 + 19*27^0 = 60,337
     *
     * But, to make the hash process faster, Horner's method should be applied as follows;
     *
     * var4*n^4 + var3*n^3 + var2*n^2 + var1*n^1 + var0*n^0 can be rewritten as
     * (((var4*n + var3)*n + var2)*n + var1)*n + var0
     *
     * Note: You must use 27 for this homework.
     *
     * However, if you have time, I would encourage you to try with other
     * constant values than 27 and compare the results but it is not required.
     * @param input input string for which the hash value needs to be calculated
     * @return int hash value of the input string
     */
	private int size;
	
	private int length;
	
	private static final DataItem DELETED = new DataItem("-1");
	
	private DataItem[] hashArray;
	
	//private double loadFactor = 0.0;
	
	public MyHashTable() {
		hashArray = new DataItem[10];
		this.size = 0;
		this.length = 10;
	}
	
	public MyHashTable(int size) {
		if (size <= 0) {
			throw new RuntimeException("size less or equal to zero");
		}
		hashArray = new DataItem[size];
		this.size = 0;
		this.length = size;
	}
	
    private int hashFunc(String input) {
    	String str = input.toLowerCase();
        //double result = 0.0;
    	int result = 0;
        int len = str.length();
        StringBuilder sb = new StringBuilder(input);
        int var = (sb.charAt(0) - 'a' + 1);
        for (int i = 0; i < len - 1; i++) {
        	var = var * 27 + (sb.charAt(i+1) - 'a' + 1);
        	var = var % this.length;
        	if (i == len - 2) {
        		result = var;
        	} else {
        		result += var;
        	}
        	//result = result + ( sb.charAt(i) - 'a' + 1) * Math.pow(27, len - 1 - i); 
        }
        return result % this.length;
    }
   

    /**
     * doubles array length and rehash items whenever the load factor is reached.
     */
    private void rehash() {
    	
    	int oldLength = this.length;
    	int length = getNewLength(this.length);
    	
    	this.length = length;
    	
    	DataItem[] oldHashArray = hashArray;
        hashArray = new DataItem[length];
        this.size = 0;
        for (int i = 0; i < oldLength; i++) {
        	if (oldHashArray[i] != null && oldHashArray[i] != DELETED ) {
        		for (int j = 0; j < oldHashArray[i].frequency; j++) {
        			insert(oldHashArray[i].value);
        		}
        	}
        }
        System.out.println("Rehashing " + this.size + " items, new size is " + this.length);
    }

    /**
     * private static data item nested class.
     */
    private static class DataItem {
        /**
         * String value.
         */
        private String value;
        /**
         * String value's frequency.
         */
        private int frequency;

        // TODO implement constructor and methods
        DataItem(String k) {
        	value = k;
        	frequency = 0;
        }
    }
    
    @Override
	public String remove(String key) {
		int hashVal = hashFunc(key);
		while (hashArray[hashVal] != null) {
			if (hashArray[hashVal].value.equals(key)) {
				String result = hashArray[hashVal].value;
				hashArray[hashVal] = DELETED;
				this.size--;
				return result;
			}
			hashVal++;
			hashVal = hashVal % hashArray.length;
		}
		return null;
	}
    
	@Override
	public void insert(String value) {
		//value = value.toLowerCase();
		if (value == null || value.isEmpty()) {
			return;
		}
		if (!isAllLowerCase(value)) {
			return;
		}
		if (contains(value)) {
			int hashVal = hashFunc(value);
			while( hashArray[hashVal] != null) {
				if (hashArray[hashVal].value.equals(value)) {
					hashArray[hashVal].frequency++;
				}
				hashVal++;
				hashVal = hashVal % this.length;
			}
			return;
		}
		this.size++;
		
		DataItem item = new DataItem(value);
		item.frequency = 1;
		int hashVal = hashFunc(value);
		while(hashArray[hashVal] != null && (hashArray[hashVal] != DELETED)) {
			hashVal++;
			hashVal = hashVal % this.length;
		}
		
		hashArray[hashVal] = item;
		
		double loadFactor = getLoadFactor();
		
		if (loadFactor > 0.5) {
			rehash();
		}
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public void display() {
		for (int i = 0; i < this.length; i++) {
			if (hashArray[i] == null) {
				System.out.print("** ");
			} else if (hashArray[i] == DELETED ) {
        		System.out.print("#DEL ");
        	} else {
        		System.out.print("[" + hashArray[i].value + ", " + hashArray[i].frequency + "] ");
        	}
        }
		System.out.println();
	}

	@Override
	public boolean contains(String key) {
		int hashVal = hashFunc(key);
		while( hashArray[hashVal] != null) {
			if (hashArray[hashVal].value.equals(key)) {
				return true;
			}
			hashVal++;
			hashVal = hashVal % this.length;
		}
		return false;
	}

	@Override
	public int numOfCollisions() {
		
		String[] keys = new String[this.size];
		
		int k = 0;
		for (int i = 0; i < this.length; i++) {
        	if (hashArray[i] != null && hashArray[i] != DELETED ) {
        		keys[k] = hashArray[i].value;
        		k++;
        	}
        }
		
		DataItem[] hashArray1 = new DataItem[this.length];
		int numberOfCollisions = 0;
        for (int i = 0; i < keys.length; i++) {
            DataItem newItem = new DataItem(keys[i]);
            
            int hashVal = hashFunc(keys[i]);
            
            if (hashArray1[hashVal] == null) {
                hashArray1[hashVal] = newItem;
            } else {
                numberOfCollisions++;
            }
        }
		return numberOfCollisions;
	}

	@Override
	public int hashValue(String value) {
		return hashFunc(value);
	}

	@Override
	public int showFrequency(String key) {
		if (!isAllLowerCase(key)) {
			return 0;
		}
		int hashVal = hashFunc(key);
		while (hashArray[hashVal] != null) {
			if (hashArray[hashVal].value.equals(key)) {
				return hashArray[hashVal].frequency;
			}
			hashVal++;
			hashVal = hashVal % hashArray.length;
		}
		return 0;
	}

	
	
	private int getNewLength(int length) {
		length = length * 2;
		int i = length;
		while( !isPrime(i)) {
			i++;
		}
		return i;
	}
	
	private boolean isPrime(int a) {  
		  
        boolean flag = true;  
  
        if (a < 2) {  
            return false;  
        } else {  
  
            for (int i = 2; i <= Math.sqrt(a); i++) {  
  
                if (a % i == 0) {  
  
                    flag = false;  
                    break;  
                }  
            }  
        }  
        return flag;  
    }
	private double getLoadFactor() {
		//System.out.println(this.size + "afas "+ this.length);
		double loadFactor = (double)this.size / this.length;
		//System.out.println("load is " + loadFactor);
		return loadFactor;
	}
	
	
	private boolean isAllLowerCase(String word) {
		StringBuilder sb = new StringBuilder(word);
		int len = sb.length();
		for (int i = 0; i < len; i++) {
			if (!(sb.charAt(i) >= 'a' && sb.charAt(i) <= 'z')) {
				return false;
			}
		}
		return true;
	}

	
}
