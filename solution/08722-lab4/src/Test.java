public class Test {

	private static int[] hashArray = new int[23]; 
	
	public static void main(String[] args) {
		String in = "ourselves";
		int result = hashFunc(in);
		System.out.println(result);

	}
	
	private static int hashFunc(String input) {
    	String str = input.toLowerCase();
        //double result = 0.0;
    	int result = 0;
        int len = str.length();
        StringBuilder sb = new StringBuilder(input);
        int var = (sb.charAt(0) - 'a' + 1);
        for (int i = 0; i < len - 1; i++) {
        	var = var * 27 + (sb.charAt(i+1) - 'a' + 1);
        	var = var % hashArray.length;
        	System.out.println(var);
        	if (i == len -1) {
        		break;
        	}
        	result += var;
        	 
        }
        return result % hashArray.length;
    }


}
