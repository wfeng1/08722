/*
 * andrewId wfeng1
 * @author Wei Feng
 */

public class SortedLinkedList implements MyListInterface {
	private static class Node {
		private String words;
		private Node next;
		
		Node(String w, Node n) {
			words = w;
			next = n;
		}
	};
	
	private Node head;
	 
	SortedLinkedList(){
		head = null;
	}
	SortedLinkedList(String[] unsorted){
		
		for(int i = 0; i < unsorted.length; i++) {
			add(unsorted[i]);
		}
		sortLink(head);
	}
	
	private Node findMin(Node in, Node min) {
		if (in == null) {
			return min;
		}
		if (in.words.compareTo(min.words) < 0) {
			min = in;
		}
		return findMin(in.next, min);
	}
	
	private void sortLink(Node out) {
		if (out == null) {
			return;
		}
		Node min = findMin(out.next, out);
		swap(min,out);
		sortLink(out.next);
	}
	
	private void swap(Node one, Node two) {
		String tmp = one.words;
		one.words = two.words;
		two.words = tmp;
	}
	private Node getLastNode(Node cur) {
		if (cur.next == null) {
			return cur;
		} 
		return getLastNode(cur.next);
	}
	@Override
	public void add(String value) {
		if (head == null) {
			head = new Node(value,null);
			return;
		}
		
		if (contains(value)) {
			return;
		}
		//while( tmp.next != null) {
			//tmp = tmp.next;
		//}
		Node tmp = getLastNode(head);
		
		tmp.next = new Node(value,null);
		sortLink(head);
	}
	private int getSize(Node cur) {
		if (cur == null) {
			return 0;
		}
		return getSize(cur.next) + 1;
	}

	@Override
	public int size() {
		return getSize(head);
	}

	private void print(Node cur) {
		if (cur.next == null) {
			System.out.println(cur.words+"]");
			return;
		}
		System.out.print(cur.words+", ");
		print(cur.next);
	}
	@Override
	public void display() {
		Node cur = head;
		System.out.print("["+head.words+", ");
		cur = cur.next;
		print(cur);
	}
	private boolean isContains(String key, Node cur) {
		if (cur == null) {
			return false;
		}
		if (cur.words.equals(key)) {
			return true;
		}
		if (isContains(key, cur.next)) {
			return true;
		}
		return false;
	}
	@Override
	public boolean contains(String key) {
		
		if (isContains(key, head)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isEmpty() {
		return head == null;
	}

	@Override
	public String removeFirst() {
		if (head == null) {
			return null;
		}
		String value = head.words;
		head = head.next;
		return value;
	}
	
	private Node getRemoveNode(int index, Node cur, int i) {
		if (cur == null) {
			return null;
		}
		if (i == index) {
			return cur;
		}
		
		return getRemoveNode(index, cur.next, i+1);
	}
	
	private Node getPrevRemoveNode(int index, Node prev, int i) {
		if (prev == null) {
			return null;
		}
		if (i == index -1) {
			return prev;
		}
		
		return getPrevRemoveNode(index, prev.next, i+1);
	}

	@Override
	public String removeAt(int index) {
		if (head == null) {
			return null;
		}
		
		if (index == 0) {
			String value = head.words;
			head = head.next;
			return value;
		}
		//int i = 0;
		//Node cur = head;
		//Node prev = null;
		
		//while(i < index && cur != null) {
			//prev = cur;
			//cur = cur.next;
			//i++;
		//}
		Node cur = getRemoveNode(index, head, 0);
		Node prev = getPrevRemoveNode(index, head, 0);
		if (cur != null && prev != null) {
			prev.next = cur.next;
			return cur.words;
		} else {
			return null;
		}
		
	}
}
