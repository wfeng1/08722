import java.util.Arrays;

public class TestArray {
	public static int removeDups(String[] array){
		 int size = array.length;
		 int len = size;
		 boolean dup = false;
		 int index = 1;
		 for (int j = 1; j < len; j++) {
			 for (int i = 0 ; i < j; i++) {
				 if (array[i].equals(array[j])) {
					 dup = true;
				 }
			 }
			 if (!dup) {
				 array[index++] = array[j];
			 }
			 dup = false;
		 }
		 return index;
	}
	public static void main(String[] args) {
		String[] a = {"aa","bb","cc","aa","aa","cc","sds"};
		System.out.println(Arrays.toString(a));
		System.out.println(removeDups(a));
	}
}
