/**
 * MyArray class.
 * @author Name:Wei Feng AndrewID: wfeng1
 */
public class MyArray {
    /**
     * Instance variable for array.
     */
    private String[] array;
    /**
     * Instance variable for size.
     */
    private int size;
    /**
     * Instance variable for capacity.
     */
    private int capacity;
    /**
     * Constructor to MyArray.
     */
    public    MyArray() {
         this.array = new String[10];
         this.size = 0;
         this.capacity = 10;
    }
    /**
     * Constructor to MyArray.
     * @param initialCapacity capacity value of array
     */
    public MyArray(int initialCapacity) {
        if (initialCapacity < 0) {
            throw new IllegalArgumentException("Illegal Capacity: " + initialCapacity);
        }
        this.array = new String[initialCapacity];
        this.size = 0;
        this.capacity = initialCapacity;
    }
    /**
     * Add Method.
     * @param text text value of word
     */
    public void add(String text) {
        if (isWord(text)) {
            ensureCapacity(this.size + 1);
            this.array[this.size] = text;
            this.size++;
        }
    }
    /**
     * search Method.
     * @param key key value of search
     * @return  is found
     */
    public boolean search(String key) {
        if (key == null) {
            return false;
        }
        if (this.size > 0) {
            for (int i = 0; i < this.size; i++) {
                if (this.array[i].equals(key)) {
                    return true;
                }
            }
        }
        return false;
    }
    /**
     * size Method.
     * @return  size
     */
    public int size() {
        return this.size;
    }
    /**
     * capacity Method.
     * @return  capacity
     */
    public int getCapacity() {
        return this.capacity;
    }
    /**
     * display Method.
     */
    public void display() {
        for (int i = 0; i < this.size  - 1; i++) {
            System.out.print(this.array[i] + " ");
        }
        System.out.println(this.array[this.size - 1]);
    }
    /**
     * removeDups Method.
     */
    public void removeDups() {
        boolean dup = false;
         int index = 1;
         for (int j = 1; j < this.size; j++) {
             for (int i = 0; i < j; i++) {
                 if (array[i].equals(array[j])) {
                     dup = true;
                 }
             }
             if (!dup) {
                 array[index++] = array[j];
             }
             dup = false;
         }
         this.size = index;
    }
    /**
     * search Method.
     * @param minCapacity minCapacity value of array
     */
    private void ensureCapacity(int minCapacity) {
        int oldCapacity = array.length;
        if (minCapacity > oldCapacity) {
            String[] oldData = array;
            int newCapacity = oldCapacity * 2;
            if (newCapacity < minCapacity) {
                newCapacity = minCapacity;
            }
            this.capacity = newCapacity;
            this.array = new String[this.capacity];
            System.arraycopy(oldData, 0, this.array, 0, this.size);
        }
    }
    /**
     * isWord Method.
     * @param text text value of word
     * @return is word
     */
    private boolean isWord(String text) {
        if (text == null || text.isEmpty()) {
            return false;
        }
        int len = text.length();
        for (int i = 0; i < len; i++) {
            if (text.charAt(i) > 'z' || text.charAt(i) < 'a') {
                if (text.charAt(i) > 'Z' || text.charAt(i) < 'A') {
                    return false;
                }
            }
        }
        return true;
    }
}
