import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
/**
 * 08722 Data Structures for Application Programmers.
 *
 * Homework Assignment 5
 * HashTable Implementation with linear probing
 *
 * Andrew ID: wfeng1
 * @author Wei Feng
 */
public class Similarity {
    /**
    * map.
    */
    private Map<String, BigInteger> map =  new HashMap<String, BigInteger>();
    /**
    * numOfWord.
    */
    private BigInteger numOfWord = new BigInteger("0");
    /**
    * line.
    */
    private int line = 0;
    /**
     * find minimal Node.
     * @param string string
    */
    public Similarity(String string) {
        if (string != null && !string.isEmpty()) {
            init(string);
        }
    }
    /**
     * find minimal Node.
     * @param string string
    */
    private void init(String string) {
        String[] str = string.split("\\W");
        int len = str.length;
        for (int i = 0; i < len; i++) {
            if (!isAlpha(str[i])) {
                continue;
            }
            String word = str[i].toLowerCase();
            this.numOfWord = numOfWord.add(BigInteger.ONE);
            if (this.map.containsKey(word)) {
                BigInteger count = this.map.get(word);
                count = count.add(BigInteger.ONE);
                this.map.put(word, count);
            } else {
                this.map.put(word, BigInteger.ONE);
            }
        }
    }
    /**
     * find minimal Node.
     * @param file string
    */
    public Similarity(File file) {
        if (file != null) {
            try {
                Scanner scanner = null;
                String in = null;
                scanner = new Scanner(file, "latin1");
                while (scanner.hasNextLine()) {
                    in = scanner.nextLine();
                    this.line++;
                    init(in);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    /**
     * find minimal Node.
     * @return int
    */
    public int numOfLines() {
        if (this.map == null || this.map.isEmpty()) {
            return 0;
        }
        return this.line;
    }
    /**
     * find minimal Node.
     * @return BigInteger
    */
    public BigInteger numOfWords() {
        return this.numOfWord;
    }
    /**
     * find minimal Node.
     * @return BigInteger
    */
    public int numOfWordsNoDups() {
        return this.map.size();
    }
    /**
     * find minimal Node.
     * @return double
    */
    public double euclideanNorm() {
        if (this.map == null || this.map.isEmpty()) {
            return 0.0;
        }
        double euclidean = 0.0;
        for (Map.Entry<String, BigInteger> e : map.entrySet()) {
            euclidean +=  e.getValue().doubleValue() * e.getValue().doubleValue();
        }
        return Math.sqrt(euclidean);
    }
    /**
     * find minimal Node.
     * @param newMap newMap
     * @return double
    */
    public double dotProduct(Map<String, BigInteger> newMap) {
        if (this.map == null || this.map.isEmpty()) {
            return 0.0;
        }
        if (newMap == null || newMap.isEmpty()) {
            return 0.0;
        }
        double dot = 0.0;
        for (Map.Entry<String, BigInteger> e : map.entrySet()) {
            if (newMap.containsKey(e.getKey())) {
                dot += e.getValue().doubleValue() * newMap.get(e.getKey()).doubleValue();
            }
        }
        return dot;
    }
    /**
     * find minimal Node.
     * @param newMap newMap
     * @return double
    */
    public double distance(Map<String, BigInteger> newMap) {
        if (newMap == null || newMap.isEmpty()) {
            return Math.acos(0);
        }
        if (this.map == null || this.map.isEmpty()) {
            return Math.acos(0);
        }
        if (this.map.equals(newMap)) {
            return 0.0;
        }
        double dot = dotProduct(newMap);
        double euclidean = euclideanNorm();
        double newEuclidean = 0.0;
        for (Map.Entry<String, BigInteger> e : newMap.entrySet()) {
            newEuclidean +=  e.getValue().doubleValue() * e.getValue().doubleValue();
        }
        newEuclidean = Math.sqrt(newEuclidean);
        return Math.acos((dot / (euclidean * newEuclidean)));
    }
    /**
     * find minimal Node.
     * @return Map<String, BigInteger>
    */
    public Map<String, BigInteger> getMap() {
        return new HashMap<String, BigInteger>(this.map);
    }
    /**
     * find minimal Node.
     * @param word word
     * @return boolean
    */
    private boolean isAlpha(String word) {
        return word.matches("[a-zA-Z]+");
    }
}
