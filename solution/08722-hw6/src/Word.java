import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Word implements Comparable<Word> {
    private String word;
    private Set<Integer> index;
    private int frequency;
    
	public Word(String str) {
    	this.word = str;
    	this.frequency = 1;
    	this.index = new HashSet<Integer>();
    }
	public String getWord() {
		return this.word;
	}
	
	public int getFrequency() {
		return frequency;
	}
	
	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}
	
	@Override
	public int compareTo(Word arg0) {
		
		return arg0.word.compareTo(this.word);
	}
	
	@Override
	public String toString() {
		return word + " " + frequency + " " + index;
	}

	public void	addToIndex(Integer line) {
		index.add(line);
	}
	public Set<Integer>	getIndex() {
		return index;
		
	}
	 
    // TODO implement methods below.
}
