import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Stack;

public class BST<T extends Comparable<T>> implements Iterable<T>, BSTInterface<T> {
    private Node<T> root;
    private Comparator<T> comparator;

    public BST() {
        this(null);
    }

    public BST(Comparator<T> comp) {
        comparator = comp;
        root = null;
    }

    public Comparator<T> comparator() {
        return comparator;
    }

    public T getRoot() {
    	if (root == null) {
    		return null;
    	}
        return root.data;
    }
    private int hight(Node<T> node) {
    	if (node == null) {
    		return -1;
    	}
    	
    	int left = hight(node.left);
    	int right = hight(node.right);
    	return Math.max(left, right) + 1;
    }
    public int getHeight() {
    	if (root == null) {
    		return 0;
    	}
    	return hight(root); 
        //throw new RuntimeException("Implement this recursively");
    }
    public int dfs(Node<T> root, ArrayList<T> list ) {
    	if (root == null) {
    		return 0;
    	}
    	list.add(root.data);
    	int left = dfs(root.left, list);
    	int right = dfs(root.right,list);
    	return left + right + 1;
    }
    public int getNumberOfNodes() {
    	ArrayList<T> list = new ArrayList<T>();
        return dfs(root,list);
    }
    
    private Node<T> searchHelp(Node<T> root, T toSearch) {
    	if (root == null) {
    		return null;
    	}
    	int temp = 0;
        if (comparator == null) {
        	temp = root.data.compareTo(toSearch);
        } else {
        	temp = comparator.compare(root.data, toSearch);
        }
    	if (temp == 0) {
    		return root;
    	}
    	
    	Node<T> left  = searchHelp(root.left, toSearch);
    	Node<T> right = searchHelp(root.right, toSearch);
    	if (left != null) {
    		return left;
    	}
    	if (right != null) {
    		return right;
    	}
    	return null;
    }
    
    @Override
    public T search(T toSearch) {
        if (root == null) {
        	return null;
        }
        Node<T> cur = searchHelp(root,toSearch);
       /* Node<T> cur = root;
        int temp = 0;
        if (comparator == null) {
        	temp =cur.data.compareTo(toSearch);
        } else {
        	temp = comparator.compare(cur.data, toSearch);
        }
         
    	while(temp!=0) {
    		if (cur.data.compareTo(toSearch) < 0) {
    			cur = cur.right;
    		} else {
    			cur = cur.left;
    		}
    		if (cur == null) {
    			return null;
    		}
    		if (comparator == null) {
            	temp =cur.data.compareTo(toSearch);
            } else {
            	temp = comparator.compare(cur.data, toSearch);
            }
    	}*/
        if (cur == null) {
        	return null;
        }
    	return cur.data;
    }
    private void insertHelp( Node<T> cur,T toInsert) {
    	int temp = 0;
		if (comparator == null) {
        	temp =cur.data.compareTo(toInsert);
        } else {
        	temp = comparator.compare(cur.data, toInsert);
        }
		if (temp == 0) {
			return;
		}
		Node<T> parent = cur;
		int temp2 = 0;
		if (comparator == null) {
        	temp2 =cur.data.compareTo(toInsert);
        } else {
        	temp2 = comparator.compare(cur.data, toInsert);
        }
		if (temp2 < 0) {
			cur = cur.right;
			if (cur == null) {
				parent.right = new Node<T>(toInsert, null, null);
				return;
			}
		} else {
			cur = cur.left;
			if (cur == null) {
				parent.left = new Node<T>(toInsert, null, null);
				return;
			}
		}
		insertHelp(cur, toInsert);
		
    }
    @Override
    public void insert(T toInsert) {
    	if (root == null) {
    		root = new Node<T>(toInsert, null, null);
    		root.data = toInsert;
    		root.left = null;
    		root.right = null;
    		return;
    	}
    	insertHelp(root, toInsert);
    	/*Node<T> parent = root;
    	Node<T> cur = root;
    	while(true) {
    		int temp = 0;
    		if (comparator == null) {
            	temp =cur.data.compareTo(toInsert);
            } else {
            	temp = comparator.compare(cur.data, toInsert);
            }
    		if (temp == 0) {
    			return;
    		}
    		parent = cur;
    		int temp2 = 0;
    		if (comparator == null) {
            	temp2 =cur.data.compareTo(toInsert);
            } else {
            	temp2 = comparator.compare(cur.data, toInsert);
            }
    		if (temp2 < 0) {
    			cur = cur.right;
    			if (cur == null) {
    				parent.right = new Node<T>(toInsert, null, null);
    				return;
    			}
    		} else {
    			cur = cur.left;
    			if (cur == null) {
    				parent.left = new Node<T>(toInsert, null, null);
    				return;
    			}
    		}
    		
    		if (comparator == null) {
            	temp =cur.data.compareTo(toInsert);
            } else {
            	temp = comparator.compare(cur.data, toInsert);
            }
    	}*/
        //throw new RuntimeException("Implement this recursively");
    }

    @Override
    public Iterator<T> iterator() {
    	class result implements Iterator<T> {
    		private Stack<Node<T>> stack = new Stack<Node<T>>();
    		private Node<T> cur = root;
			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return (cur != null || !stack.isEmpty());
			}

			@Override
			public T next() {
				while(cur!=null) {
					stack.push(cur);
					cur = cur.left;
				}
				cur = stack.pop();
				Node<T> node = cur;
				cur = cur.right;
				return node.data;
			}
    		
    	}
    	
    	return new result();
    }

    private static class Node<T> {
        private T data;
        private Node<T> left;
        private Node<T> right;

        Node(T data) {
            this(data, null, null);
        }

        Node(T d, Node<T> l, Node<T> r) {
            data = d;
            left = l;
            right = r;
        }

        @Override
        public String toString() {
            return data.toString();
        }
    }

    public ArrayList<T> getAllNodes() {
    	ArrayList<T> list = new ArrayList<T>();
    	dfs(root, list);
    	return list;
    }
}