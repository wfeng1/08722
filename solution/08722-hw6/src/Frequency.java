import java.util.Comparator;

public class Frequency implements Comparator<Word> {

	@Override
	public int compare(Word o1, Word o2) {
		
		return o2.getFrequency() - o1.getFrequency();
	}

}
