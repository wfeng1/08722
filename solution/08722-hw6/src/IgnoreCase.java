import java.util.Comparator;

public class IgnoreCase implements Comparator<Word> {

	@Override
	public int compare(Word arg0, Word arg1) {
		
		return arg1.getWord().toLowerCase().compareTo(arg0.getWord().toLowerCase());
	}

}
