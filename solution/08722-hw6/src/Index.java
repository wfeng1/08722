import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Index {

    public BST<Word> buildIndex(String fileName) {
    	BST<Word> tree = new BST<Word>();
    	try {
    		Scanner scanner = null;
            String in = null;
            int line = 1;
            Set<String> map = new HashSet<String>();
            scanner = new Scanner(new File(fileName), "latin1");
            while (scanner.hasNextLine()) {
                in = scanner.nextLine();
                String[] re = in.split("\\W");
                int len = re.length;
                for (int i = 0; i < len; i++) {
                	if (!isAlpha(re[i])) {
                		continue;
                	}
                	if (map.contains(re[i])) {
                		Word word = tree.search(new Word(re[i]));
                		word.setFrequency(word.getFrequency() + 1);
                		
                		word.addToIndex(line);
                		tree.insert(word);
                	} else {
                		Word word = new Word(re[i]);
                		word.addToIndex(line);
                    	tree.insert(word);
                    	map.add(re[i]);
                	}
                }
                line++;
			}
		} catch ( IOException e) {
			
			e.printStackTrace();
		}
    	
    	return tree;
       // throw new RuntimeException("Implement this!");
    }

    public BST<Word> buildIndex(String fileName, Comparator<Word> comparator) {
    	BST<Word> tree = new BST<Word>(comparator);
    	try {
    		Scanner scanner = null;
            String in = null;
            int line = 1;
            Set<String> map = new HashSet<String>();
            scanner = new Scanner(new File(fileName), "latin1");
            while (scanner.hasNextLine()) {
                in = scanner.nextLine();
                String[] re = in.split("\\W");
                int len = re.length;
                for (int i = 0; i < len; i++) {
                	if (!isAlpha(re[i])) {
                		continue;
                	}
                	re[i] = re[i].toLowerCase();
                	if (map.contains(re[i])) {
                		Word word = tree.search(new Word(re[i]));
                	
                		word.setFrequency(word.getFrequency() + 1);
                		
                		word.addToIndex(line);
                		tree.insert(word);
                	} else {
                	
                		Word word = new Word(re[i]);
                		word.addToIndex(line);
                    	tree.insert(word);
                    	map.add(re[i]);
                	}
                }
                line++;
			}
		} catch ( IOException e) {
			
			e.printStackTrace();
		}
    	
    	return tree;
    }

    public BST<Word> buildIndex(ArrayList<Word> list, Comparator<Word> comparator) {
    	BST<Word> tree = new BST<Word>(comparator);
    	for (Word w:list) {
        	tree.insert(w);
        }
    	return tree;
    }

    
    public ArrayList<Word> sortByAlpha(BST<Word> tree) {
        
    	ArrayList<Word> list = new ArrayList<Word>();
    	list = tree.getAllNodes();
    	Collections.sort(list, new Comparator<Word>() {

			@Override
			public int compare(Word o1, Word o2) {
				return o2.compareTo(o1);
			}
    		
    	});
    	
    	
    	return list;
    	//throw new RuntimeException("Implement this!");
    }

    public ArrayList<Word> sortByFrequency(BST<Word> tree) {
    	ArrayList<Word> list = new ArrayList<Word>();
    	list = tree.getAllNodes();
    	Collections.sort(list, new Comparator<Word>() {

			@Override
			public int compare(Word o1, Word o2) {
				if (o2.getFrequency() - o1.getFrequency() == 0) {
					return o2.compareTo(o1);
				} else {
					return o2.getFrequency() - o1.getFrequency();
				}
			}
    		
    	});
    	
    	
    	return list;
    }

    public ArrayList<Word> getHighestFrequency(BST<Word> tree) {
        ArrayList<Word> list = sortByFrequency(tree);
        ArrayList<Word> re = new ArrayList<Word>();
        int highest = list.get(0).getFrequency();
        for (Word w : list) {
        	if (w.getFrequency() != highest) {
        		break;
        	}
        	re.add(w);
        }
        return re;
    }
    private boolean isAlpha(String word) {
        return word.matches("[a-zA-Z]+");
    }
}
