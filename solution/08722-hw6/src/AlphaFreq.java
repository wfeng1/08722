import java.util.Comparator;

public class AlphaFreq implements Comparator<Word> {

	@Override
	public int compare(Word o1, Word o2) {
		if (o2.getWord().compareTo(o1.getWord()) == 0 ) {
			return o2.getFrequency() - o1.getFrequency(); 
		} else {
			return o2.getWord().compareTo(o1.getWord());
		}
	}

}
